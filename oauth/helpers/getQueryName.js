import gql from 'graphql-tag';

export default function (request) {
  //  console.log(request.body);

  let name = '';

  if (request.body.query) {
    const text = gql`
            ${request.body.query}
        `;

    name = text.definitions[0].selectionSet.selections[0].name.value;
  } else if (request.body.mutation) {
    const text = gql`
            ${request.body.mutation}
        `;

    name = text.definitions[0].selectionSet.selections[0].name.value;
  }

  return name;
}
